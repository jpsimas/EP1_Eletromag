#!/usr/bin/env python
import numpy
import matplotlib.pyplot as plot
EPSILON_R = 1.9
EPSILON_0 = 8.854187817e-12
SIGMA =  3.2e-3
ESPESSURA = 1
DELTA = 1e-4
V = 100

#Lê arquivos gerados pela simulação
phi = numpy.loadtxt("out/potencial.csv",delimiter=",", dtype=numpy.float64)
isBorder = numpy.loadtxt("out/condutor.csv",delimiter=",", dtype=numpy.bool_)

sizeX = phi.shape[0]
sizeY = phi.shape[1]

Ex = numpy.zeros(phi.shape)
Ey = numpy.zeros(phi.shape) 
densidadeSuperficial = numpy.zeros(phi.shape)                

#Cálculo de E
for i in range(1,sizeX-1):
  for j in range(1, sizeY-1):
    Ex[i, j] = -(phi[i+1, j] - phi[i-1, j])/(2*DELTA)
    Ey[i, j] = -(phi[i, j+1] - phi[i, j-1])/(2*DELTA)

for j in range(0, sizeY):
  Ex[0, j] = -(phi[1, j] - phi[0, j])/DELTA
  Ex[sizeX-1, j] = -(phi[sizeX-1, j] - phi[sizeX-2, j])/DELTA
  if(j != 0 and j != sizeY - 1):
    Ey[0, j] = -(phi[0, j+1] - phi[0, j-1])/(2*DELTA)
    Ey[sizeX-1, j] = -(phi[sizeX-1, j+1] - phi[sizeX-1, j-1])/(2*DELTA)

for i in range(0, sizeX):
  Ey[i, 0] = -(phi[i, 1] - phi[i, 0])/DELTA
  Ey[i, sizeY-1] = -(phi[i, sizeY-1] - phi[i, sizeY-2])/DELTA
  if(i != 0 and i != sizeX - 1):
    Ex[i, 0] = -(phi[i+1, 0] - phi[i-1, 0])/(2*DELTA)
    Ex[i, sizeY-1] = -(phi[i+1, sizeY-1] - phi[i-1, sizeY-1])/(2*DELTA)

#Cálculo da densidade superficial
dirX = 0
dirY = 0
for i in range(0, sizeX):
  for j in range(0, sizeY):
    dirX = 0
    dirY = 0
    if(isBorder[i, j]):
      if(i < sizeX - 1):
        if(not isBorder[i+1, j]):
          dirX+=1
      if(j < sizeY - 1):
        if(not isBorder[i, j+1]):
          dirY+=1
      if(i > 0):
        if(not isBorder[i-1, j]):
          dirX-=1
      if(j > 0):
        if(not isBorder[i, j-1]):
          dirY-=1
      if((dirX and (not dirY)) or ((not dirX) and dirY)):
        densidadeSuperficial[i, j] = EPSILON_0*EPSILON_R*(dirX*Ex[i+dirX, j] + dirY*Ey[i, j+dirY])
      else:
        densidadeSuperficial[i, j] = 0

print("Densidade Mínima: ")
print(numpy.amin(densidadeSuperficial))

print("Fluxo: ")

F = 0
for i in range(1, sizeX-1):
  F += Ey[i, 0]*DELTA*ESPESSURA
  F -= Ey[i, sizeY-1]*DELTA*ESPESSURA
for j in range(1, sizeY-1):
  F += Ex[0, j]*DELTA*ESPESSURA
  F -= Ex[sizeX-1, j]*DELTA*ESPESSURA
print(F)

F = abs(F)
  
print("Resistência (ohms): ")
R = V/(SIGMA*F)
print(R)

print("Capacitância: (pF)")
print(1e12*(F*EPSILON_0*EPSILON_R)/V)

print("R' (ohms): ")
print(1/(2*SIGMA*SIGMA*ESPESSURA*ESPESSURA*R))

#Plota grafico com equipotenciais
a, b = plot.subplots()
X = numpy.mgrid[0:11e-2:DELTA]
Y = numpy.mgrid[0:6e-2:DELTA]
cont = b.contour(X,Y,phi, [0,10,20,30,40,50,60,70,80,90,99.9999999], colors = 'black')

plot.clabel(cont, inline=0, fontsize=7)
plot.xticks(numpy.mgrid[0:0.12:0.002], numpy.mgrid[0:0.12:0.002])
plot.yticks(numpy.mgrid[0:0.6:0.002], numpy.mgrid[0:0.6:0.002])
b.set_xlim([0,0.11])
b.set_ylim([0,0.06])
plot.gca().set_aspect('equal', adjustable='box')
plot.grid()
plot.show()
