#include "Grid2D.h"
#include <string>
#include <fstream>
#include <math.h> //round
#include <limits>

//Professor: Leb, nusp: 9349429
#define A 11e-2
#define B 6e-2
#define C 5e-2
#define D 2e-2
#define G 3e-2
#define H 2e-2 
#define DELTA 1e-4 //tamanho da divisão
#define ITERATIONS 500e4
#define div(x) round(x/DELTA) //converte de metros para divisões

void csvexport(double** data, int sizeX, int sizeY,std::string filename);
void csvexportbool(bool** data, int sizeX, int sizeY,std::string filename);

int main(int argc, char** argv){
  int tamanhoX = div(A);
  int tamanhoY = div(B);
  double** gridData = new double*[tamanhoX];
  bool** condutor = new bool*[tamanhoX];

  for(int i = 0; i < tamanhoX; i++){
    gridData[i] = new double[tamanhoY];
    condutor[i] = new bool[tamanhoY];
  }
  
  //Faz a tensão no condutor interno 100V, 0V no resto e marca regioes condutoras
  for(int i = 0; i < tamanhoX; i++)
    for(int j = 0; j < tamanhoY; j++){
      if((i >= div(G)) && i <= (div(C) + div(G)) && j >= div(H) && j <= (div(D) + div(H))){
	gridData[i][j] = 100;
	condutor[i][j] = 1;
      } else {
	gridData[i][j] = 0;
	if(i == 0 || i == tamanhoX - 1 || j == 0 || j == tamanhoY - 1)
	  condutor[i][j] = 1;
	else
	  condutor[i][j] = 0;
      }
    }
  
  Grid2D grid(gridData, condutor, tamanhoX, tamanhoY, DELTA);

  for(int i = 0; i < tamanhoX; i++)
    delete gridData[i];
  delete gridData;
  delete condutor;
  
  grid.iterate(ITERATIONS);
  csvexport(grid.getGrid(), tamanhoX, tamanhoY, "out/potencial.csv");
  csvexportbool(condutor, tamanhoX, tamanhoY, "out/condutor.csv");
  return 0;
}

void csvexport(double** data, int sizeX, int sizeY,std::string filename){
  std::ofstream file;
  file.precision(std::numeric_limits<double>::max_digits10);
  file.open(filename.c_str());
  int i, j;
  for(j = 0; j < sizeY; j++){
    for(i = 0; i < sizeX-1; i++){
      file << std::scientific << data[i][j];
      file << ",";
    }
    file << std::scientific << data[i][j];
    file << std::endl;
  }
}

void csvexportbool(bool** data, int sizeX, int sizeY,std::string filename){
  std::ofstream file;

  file.open(filename.c_str());
  int i, j;
  for(j = 0; j < sizeY; j++){
    for(i = 0; i < sizeX-1; i++){
      file << data[i][j];
      file << ",";
    }
    file << data[i][j];
    file << std::endl;
  }
}
