class Grid2D{
 private:
  double** grid;
  double** gridNext;
  bool** isBorder;
  int sizeX, sizeY;
  double delta;
  void swapGrid();
  
 public:
  Grid2D(double** initialGrid, bool** isBorder, int sizeX, int sizeY, double delta);
  ~Grid2D();
  void update();
  void iterate(int);
  double** getGrid();
};
