#include "Grid2D.h"
#include <future>
#include <vector>

Grid2D::Grid2D(double** initialGrid, bool** condutor, int sizeX, int sizeY, double delta):sizeX(sizeX), sizeY(sizeY), delta(delta){
  //Allocate grid
  grid = new double*[sizeX];
  for(int i = 0; i < sizeX; i++)
    grid[i] = new double[sizeY];
  //Initialize grid
  for(int i = 0; i < sizeX; i++)
    for(int j = 0; j < sizeY; j++)
      grid[i][j] = initialGrid[i][j];
  //Allocate gridNext
  gridNext = new double*[sizeX];
  for(int i = 0; i < sizeX; i++)
    gridNext[i] = new double[sizeY];
  //Copy grid to gridNext
  for(int i = 0; i < sizeX; i++)
    for(int j = 0; j < sizeY; j++)
      gridNext[i][j] = grid[i][j];
  //Allocate isBorder
  isBorder = new bool*[sizeX];
  for(int i = 0; i < sizeX; i++)
    isBorder[i] = new bool[sizeY];
  //Initialize isBorder
  for(int i = 0; i < sizeX; i++)
    for(int j = 0; j < sizeY; j++)
      isBorder[i][j] = condutor[i][j];
}

Grid2D::~Grid2D(){
  for(int i = 0; i < sizeX; i++){
    delete grid[i];
    delete gridNext[i];
  }
  delete grid;
  delete gridNext;
}

void Grid2D::swapGrid(){
  double** a;
  a = grid;
  grid = gridNext;
  gridNext = a;
}

void updateAux(double** grid, double** gridNext, int start, int end, int sizeY, bool** isBorder){
  for(int i = start; i < end; i++)
    for(int j = 1; j < sizeY-1; j++)
      if(!isBorder[i][j])
	gridNext[i][j] = (grid[i+1][j] + grid[i-1][j] + grid[i][j+1] + grid[i][j-1])/4;
}

void Grid2D::update(){
  int delta = sizeX/4;
  std::vector<std::future<void>> futures;
  for(int i = 0; i < 3; i++)
    futures.push_back(std::async(updateAux, grid, gridNext, i*delta, (i+1)*delta, sizeY, isBorder));
  updateAux(grid, gridNext, 3*delta, sizeX, sizeY, isBorder);
  for(auto& fut : futures)
    fut.get();
  swapGrid();
}

void Grid2D::iterate(int n){
  for(int i = 0; i < n; i++)
    update();
}

double** Grid2D::getGrid(){
  return grid;
}



